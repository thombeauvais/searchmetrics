##Searchmetrics Coding Task

Please develop a service, that constantly checks the currency exchange rate from Bitcoin to US-Dollar (1 Bitcoin = x USD).

###Requirements

The check period has to be configurable
The service must have a REST API with the following endpoints:
Get latest rate
Get historical rates from startDate to endDate
Please describe in a few sentences and/or with a diagram how you planned the project and architecture.
It should be coded in Java
There are no restrictions in terms of technologies, but please take a look at our tech-radar (https://status.searchmetrics.com/tech_radar/)

If you have any questions, please feel free to reach out to us.  

Enjoy the task! 

Searchmetrics Talent Acquisition

### Running
Build Spring using Gradle

`./gradlew build`

Then, starting Docker Compose (assuming you have it installed)

`docker-compose up --build`

### Endpoints

Please see the CryptoRestController for more detailed information on parameters and return types. 

`/tick/last`

This will return the last fetch from the crypto APIs.

`/tick/between`

This will return a list of tickets between a startDate and endDate.

`/tick/bucketByInterval`

This is an additional endpoint that will return "buckets" that have aggregated the average, minimum and 
maximum market values for an interval of time. This endpoint can take advantage of the startDate and endDate as well.