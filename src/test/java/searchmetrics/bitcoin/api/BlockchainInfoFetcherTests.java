package searchmetrics.bitcoin.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import searchmetrics.crypto.fetch.BlockchainGetInfoTicker;

@Slf4j
@SpringBootTest
public class BlockchainInfoFetcherTests extends AbstractFetcherTests {
    @Autowired
    public BlockchainInfoFetcherTests(BlockchainGetInfoTicker fetcher) {
        super(fetcher);
    }
}

