package searchmetrics.bitcoin.api;

import lombok.AllArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Configuration;
import searchmetrics.crypto.fetch.FetchResults;
import searchmetrics.crypto.fetch.Fetcher;

import static org.junit.jupiter.api.Assertions.*;
import static searchmetrics.crypto.Currency.Crypto.BTC;
import static searchmetrics.crypto.Currency.Fiat.EUR;
import static searchmetrics.crypto.Currency.Fiat.USD;

@AllArgsConstructor
public abstract class AbstractFetcherTests {
    private Fetcher fetcher;

    @Test
    public void fetch_one() {
        FetchResults results = this.fetcher.fetch(BTC, USD);

        assertNotNull(results);
        assertNotNull(results.getCrypto());
        assertTrue(results.getFiatPrice(USD) > 0);
    }

    @Test
    public void fetch_multiple() {
        FetchResults results = this.fetcher.fetch(BTC, USD, EUR);

        assertNotNull(results);
        assertNotNull(results.getCrypto());
        assertTrue(results.getFiatPrice(USD) > 0);
        assertTrue(results.getFiatPrice(EUR) > 0);
    }

    @Test
    public void fetch_one_then_check_another_to_return_negative_one() {
        FetchResults results = this.fetcher.fetch(BTC, USD);

        assertNotNull(results);
        assertNotNull(results.getCrypto());
        assertTrue(results.getFiatPrice(USD) > 0);
        assertEquals(-1, results.getFiatPrice(EUR));
    }
}
