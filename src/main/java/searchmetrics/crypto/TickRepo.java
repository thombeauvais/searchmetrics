package searchmetrics.crypto;

import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TickRepo extends ElasticsearchRepository<Tick, String> {
    List<Tick> findByCryptoOrderByDateDesc(Currency.Crypto crypto);

    List<Tick> findAllByCryptoAndDateBetweenOrderByDateDesc(Currency.Crypto crypto, Long startDate, Long endDate, Pageable pageable);
}
