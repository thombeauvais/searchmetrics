package searchmetrics.crypto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PricePair {
    Double price;
    Currency from;
    Currency.Fiat to;
}
