package searchmetrics.crypto.fetch;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import searchmetrics.crypto.Currency;
import searchmetrics.crypto.PricePair;

import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toUnmodifiableMap;

/**
 * https://cex.io/rest-api#last-price
 * <p>
 * GET https://cex.io/api/last_price/{symbol1}/{symbol2}
 * <p>
 * Response
 * {{
 * {
 * "lprice": "17663",
 * "curr1": "BTC",
 * "curr2": "USD"
 * }
 * }}
 */
@Slf4j
@Component
public class CEXGetLastPrice extends AbstractFetcher implements Fetcher {
    @Autowired
    private RestTemplate restTemplate;

    static final String URL_PATTERN = "https://cex.io/api/last_price/{symbol1}/{symbol2}";

    @Override
    public FetchResults fetch(Currency.Crypto crypto, Set<Currency.Fiat> fiats) {
        Stream<PricePair> pricePairStream = fiats.stream()
                .map((fiat) -> {
                    log.info("Calling CEX last_price API with {} -> {}", crypto, fiat);

                    ResponseEntity<CEXGetLastPriceResponse> forEntity = this.restTemplate.getForEntity(URL_PATTERN, CEXGetLastPriceResponse.class, crypto, fiat);

                    CEXGetLastPriceResponse body = forEntity.getBody();

                    // TODO handle null (if it makes sense)

                    return new PricePair(body.lprice, crypto, fiat);
                });

        Map<Currency.Fiat, Double> priceMap = pricePairStream.collect(toUnmodifiableMap(PricePair::getTo, PricePair::getPrice));

        return FetchResults.builder()
                .crypto(crypto)
                .priceMap(priceMap)
                .build();
    }

    @Data
    @NoArgsConstructor
    static class CEXGetLastPriceResponse {
        Double lprice;
        String curr1;
        String curr2;
    }
}
