package searchmetrics.crypto.fetch;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import searchmetrics.crypto.Currency;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static java.util.stream.Collectors.toUnmodifiableMap;

/**
 * https://blockchain.info/ticker
 * <p>
 * Response
 * {{code}}
 * {
 * "USD" : {"15m" : 7924.4, "last" : 7924.4, "buy" : 7924.4, "sell" : 7924.4, "symbol" : "$"},
 * "AUD" : {"15m" : 12021.31, "last" : 12021.31, "buy" : 12021.31, "sell" : 12021.31, "symbol" : "$"},
 * "BRL" : {"15m" : 36668.59, "last" : 36668.59, "buy" : 36668.59, "sell" : 36668.59, "symbol" : "R$"},
 * "CAD" : {"15m" : 10784.84, "last" : 10784.84, "buy" : 10784.84, "sell" : 10784.84, "symbol" : "$"},
 * "CHF" : {"15m" : 7349.04, "last" : 7349.04, "buy" : 7349.04, "sell" : 7349.04, "symbol" : "CHF"},
 * "CLP" : {"15m" : 6554270.11, "last" : 6554270.11, "buy" : 6554270.11, "sell" : 6554270.11, "symbol" : "$"},
 * "CNY" : {"15m" : 55071.44, "last" : 55071.44, "buy" : 55071.44, "sell" : 55071.44, "symbol" : "¥"},
 * "DKK" : {"15m" : 51927.57, "last" : 51927.57, "buy" : 51927.57, "sell" : 51927.57, "symbol" : "kr"},
 * "EUR" : {"15m" : 6968.99, "last" : 6968.99, "buy" : 6968.99, "sell" : 6968.99, "symbol" : "€"},
 * "GBP" : {"15m" : 6029.22, "last" : 6029.22, "buy" : 6029.22, "sell" : 6029.22, "symbol" : "£"},
 * "HKD" : {"15m" : 61559.87, "last" : 61559.87, "buy" : 61559.87, "sell" : 61559.87, "symbol" : "$"},
 * "INR" : {"15m" : 587416.16, "last" : 587416.16, "buy" : 587416.16, "sell" : 587416.16, "symbol" : "₹"},
 * "ISK" : {"15m" : 1002278.49, "last" : 1002278.49, "buy" : 1002278.49, "sell" : 1002278.49, "symbol" : "kr"},
 * "JPY" : {"15m" : 816519.03, "last" : 816519.03, "buy" : 816519.03, "sell" : 816519.03, "symbol" : "¥"},
 * "KRW" : {"15m" : 9514039.51, "last" : 9514039.51, "buy" : 9514039.51, "sell" : 9514039.51, "symbol" : "₩"},
 * "NZD" : {"15m" : 12534.97, "last" : 12534.97, "buy" : 12534.97, "sell" : 12534.97, "symbol" : "$"},
 * "PLN" : {"15m" : 30037.65, "last" : 30037.65, "buy" : 30037.65, "sell" : 30037.65, "symbol" : "zł"},
 * "RUB" : {"15m" : 587483.62, "last" : 587483.62, "buy" : 587483.62, "sell" : 587483.62, "symbol" : "RUB"},
 * "SEK" : {"15m" : 74449.57, "last" : 74449.57, "buy" : 74449.57, "sell" : 74449.57, "symbol" : "kr"},
 * "SGD" : {"15m" : 10966.52, "last" : 10966.52, "buy" : 10966.52, "sell" : 10966.52, "symbol" : "$"},
 * "THB" : {"15m" : 250355.7, "last" : 250355.7, "buy" : 250355.7, "sell" : 250355.7, "symbol" : "฿"},
 * "TWD" : {"15m" : 237886.64, "last" : 237886.64, "buy" : 237886.64, "sell" : 237886.64, "symbol" : "NT$"}
 * }
 * {{code}}
 */
@Slf4j
@Component
public class BlockchainGetInfoTicker extends AbstractFetcher implements Fetcher {
    @Autowired
    private RestTemplate restTemplate;

    static final String URL_PATTERN = "https://blockchain.info/ticker";

    public FetchResults fetch(Currency.Crypto crypto, Set<Currency.Fiat> fiats) {
        log.info("Calling Blockchain last_price API with {} -> {}", crypto, fiats);

        ResponseEntity<Map<String, BlockchainInfoTick>> forEntity = this.restTemplate.exchange(URL_PATTERN, HttpMethod.GET, null, new ParameterizedTypeReference<>() {
        });

        // This might not be the best choice to filter out no desired currencies
        // There might be some services that would like to fetch all (for that we can use a sentinel ALL value or something)

        Map<Currency.Fiat, Double> priceMap = Objects.requireNonNull(forEntity.getBody())
                .entrySet()
                .stream()
                .filter((e) -> Currency.Fiat.isValid.test(e.getKey())) // test to make sure we actually have registered the Fiat
                .filter((e) -> fiats.contains(Currency.Fiat.valueOf(e.getKey()))) // only add the Fiats that were asked for --- TODO this might need to change
                .collect(toUnmodifiableMap((e) -> Currency.Fiat.valueOf(e.getKey()), (e) -> e.getValue().getLast()));

        return FetchResults.builder()
                .crypto(crypto)
                .priceMap(priceMap)
                .build();
    }

    @Data
    @NoArgsConstructor
    static class BlockchainInfoTick {
        @JsonAlias("15m")
        Double _15m;
        Double last;
        Double buy;
        Double sell;
        String symbol;
    }
}
