package searchmetrics.crypto.fetch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import searchmetrics.crypto.Currency;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This is the component that everyone should use rather than specific fetchers. It will take all the
 * configured fetchers and average them together.
 */
@Component
public class AverageFetcher extends AbstractFetcher implements Fetcher {
    @Autowired
    List<Fetcher> fetchers;

    @Override
    public FetchResults fetch(Currency.Crypto crypto, Set<Currency.Fiat> fiats) {
        Map<Currency.Fiat, Double> priceMap = this.fetchers.parallelStream()
                .map((fetcher -> fetcher.fetch(crypto, fiats)))
                .map((FetchResults::getPriceMap))
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(
                        Map.Entry::getKey,
                        Collectors.averagingDouble(Map.Entry::getValue)
                ));

        return FetchResults.builder()
                .crypto(crypto)
                .fiats(fiats)
                .priceMap(priceMap)
                .build();
    }
}