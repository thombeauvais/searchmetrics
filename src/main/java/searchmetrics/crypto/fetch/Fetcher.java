package searchmetrics.crypto.fetch;

import searchmetrics.crypto.Currency;

import java.util.Set;

public interface Fetcher {
    /**
     * Fetch the current market rates from a ticker API.
     *
     * @param crypto the crypto to convert
     * @param fiats  the fiat to ask to convert to
     * @return
     */
    FetchResults fetch(Currency.Crypto crypto, Currency.Fiat... fiats);

    /**
     * Fetch the current market rates from a ticker API.
     *
     * @param crypto the crypto to convert
     * @param fiats  the fiat to ask to convert to
     * @return
     */
    FetchResults fetch(Currency.Crypto crypto, Set<Currency.Fiat> fiats);
}
