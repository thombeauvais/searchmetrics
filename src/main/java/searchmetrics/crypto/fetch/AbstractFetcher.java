package searchmetrics.crypto.fetch;

import searchmetrics.crypto.Currency;

import java.util.Arrays;
import java.util.HashSet;

public abstract class AbstractFetcher implements Fetcher {
    @Override
    public FetchResults fetch(Currency.Crypto crypto, Currency.Fiat... fiats) {
        return this.fetch(crypto, new HashSet<>(Arrays.asList(fiats)));
    }
}
