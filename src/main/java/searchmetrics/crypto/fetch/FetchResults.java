package searchmetrics.crypto.fetch;

import lombok.Builder;
import lombok.Getter;
import searchmetrics.crypto.Currency;

import java.util.Map;
import java.util.Set;

@Getter
@Builder
public class FetchResults {
    private Currency.Crypto crypto;
    private Set<Currency.Fiat> fiats;

    private Map<Currency.Fiat, Double> priceMap;

    /**
     *
     * @param fiat
     * @return the price and -1 if there is no price for the {@code fiat} parameter
     */
    public double getFiatPrice(Currency.Fiat fiat) {
        return priceMap != null && priceMap.get(fiat) != null ? priceMap.get(fiat) : -1;
    }
}
