package searchmetrics.crypto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class TickBucketResponse {
    TickBucketRequest request;

    List<TickBucket> tickBuckets;
}
