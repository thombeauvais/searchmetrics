package searchmetrics.crypto;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.AggregatorFactories;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.InternalDateHistogram;
import org.elasticsearch.search.aggregations.metrics.avg.InternalAvg;
import org.elasticsearch.search.aggregations.metrics.max.InternalMax;
import org.elasticsearch.search.aggregations.metrics.min.InternalMin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.ResultsExtractor;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.elasticsearch.search.aggregations.AggregationBuilders.*;

@Service
public class TickService {
    @Autowired
    TickRepo tickRepo;

    @Autowired
    ElasticsearchTemplate elasticsearchTemplate;

    private Tick processTick(Tick tick, Currency.Fiat[] fiats) {
        // TODO figure out how to project/filter the Fiats with Spring Data or do it manually against the ElasticsearchTemplate
        List<Currency.Fiat> fiatList = Arrays.asList(fiats);
        tick.getPriceMap().entrySet().removeIf(entry -> !fiatList.contains(entry.getKey()));

        return tick;
    }

    /**
     * @param crypto
     * @param fiats
     * @return
     */
    public Tick getLast(Currency.Crypto crypto, Currency.Fiat[] fiats) {
        // TODO do to a bug in Spring JPA, the TOP and FIRST query words are broken in Elastic
        // TODO https://jira.spring.io/browse/DATAES-176
        return processTick(this.tickRepo.findByCryptoOrderByDateDesc(crypto).get(0), fiats);
    }

    private Long getEndDateMillis(Long endDateMillis) {
        // if there is no endDate, then just use right now
        if (endDateMillis == null) {
            return System.currentTimeMillis();
        }

        return endDateMillis;
    }

    private Long getStartDate(Long startDateMillis, Long endDateMillis) {
        // if there is not startDate, then take 2 hours before the endDate
        if (startDateMillis == null) {
            ZoneId zone = ZoneId.systemDefault();

            LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(endDateMillis), zone);

            return ZonedDateTime.of(dateTime.minusHours(2), ZoneId.systemDefault()).toInstant().toEpochMilli();
        }

        return startDateMillis;
    }

    /**
     * @param crypto
     * @param fiat
     * @param startDateMillis
     * @param endDateMillis
     * @return
     */
    public List<Tick> findTicks(Currency.Crypto crypto, Currency.Fiat fiat, Long startDateMillis, Long endDateMillis) {
        Long endDate = getEndDateMillis(endDateMillis);
        Long startDate = getStartDate(startDateMillis, endDate);

        // TODO simply add paging if necessary

        return this.tickRepo.findAllByCryptoAndDateBetweenOrderByDateDesc(crypto, startDate, endDate, null)
                .stream()
                .map((tick) -> processTick(tick, new Currency.Fiat[]{fiat}))
                .collect(Collectors.toUnmodifiableList());
    }

    /**
     * @param crypto
     * @param fiat
     * @param startDateMillis
     * @param endDateMillis
     * @param interval
     * @return
     */
    public TickBucketResponse findTickBuckets(Currency.Crypto crypto,
                                              Currency.Fiat fiat,
                                              Long startDateMillis,
                                              Long endDateMillis,
                                              String interval) {

        TickBucketRequest request = TickBucketRequest.builder()
                .crypto(crypto)
                .fiat(fiat)
                .startDateMillis(startDateMillis)
                .endDateMillis(endDateMillis)
                .interval(interval)
                .build();

        return this.findTickBuckets(request);
    }

    /**
     * @param request
     * @return
     */
    public TickBucketResponse findTickBuckets(TickBucketRequest request) {
        // sourced from https://stackoverflow.com/questions/30184764/aggregation-support-for-spring-data-elastic-search

        String field = "priceMap." + request.getFiat();

        DateHistogramAggregationBuilder dateHistogram = dateHistogram("dateHistogram")
                .dateHistogramInterval(new DateHistogramInterval(request.getInterval()))
                .field("date");

        // get some useful insights to draw some cool charts
        dateHistogram.subAggregations(
                AggregatorFactories.builder()
                        .addAggregator(avg("average").field(field))
                        .addAggregator(min("min").field(field))
                        .addAggregator(max("max").field(field))
        );

        Long endDate = getEndDateMillis(request.getEndDateMillis());
        Long startDate = getStartDate(request.getStartDateMillis(), endDate);

        // this is an example of how to manually make a query (and not use Spring Data JPA)
        BoolQueryBuilder query = boolQuery()
                .must(matchQuery("crypto", request.getCrypto().toString()))
                .must(rangeQuery("date").lte(endDate))
                .must(rangeQuery("date").gte(startDate));

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withIndices(Tick.ELASTICSEARCH_INDEX)
                .withQuery(query)
                .addAggregation(dateHistogram)
                .build();

        Aggregations aggregations = elasticsearchTemplate.query(searchQuery, SearchResponse::getAggregations);

        InternalDateHistogram dateHistogramResponse = aggregations.get("dateHistogram");

        // transform
        List<TickBucket> tickBuckets = dateHistogramResponse.getBuckets()
                .stream()
                .map((entry) -> TickBucket.builder()
                        .millis(Long.parseLong(entry.getKeyAsString()))
                        .average(((InternalAvg) entry.getAggregations().get("average")).getValue())
                        .min(((InternalMin) entry.getAggregations().get("min")).getValue())
                        .max(((InternalMax) entry.getAggregations().get("max")).getValue())
                        .build())
                .collect(Collectors.toUnmodifiableList());

        return TickBucketResponse.builder()
                .request(request)
                .tickBuckets(tickBuckets)
                .build();
    }
}
