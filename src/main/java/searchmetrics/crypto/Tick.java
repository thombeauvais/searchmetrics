package searchmetrics.crypto;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Date;
import java.util.Map;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = Tick.ELASTICSEARCH_INDEX)
public class Tick {
    public static final String ELASTICSEARCH_INDEX = "tick";
    @Id
    String id;

    Date date;

    Currency.Crypto crypto;

    Map<Currency.Fiat, Double> priceMap;
}
