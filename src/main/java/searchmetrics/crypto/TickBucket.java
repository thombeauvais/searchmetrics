package searchmetrics.crypto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TickBucket {
    Long millis;
    Double average;
    Double min;
    Double max;
}
