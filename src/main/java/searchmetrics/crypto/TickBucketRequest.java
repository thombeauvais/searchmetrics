package searchmetrics.crypto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TickBucketRequest {
    Currency.Crypto crypto;
    Currency.Fiat fiat;
    Long startDateMillis;
    Long endDateMillis;
    String interval;
}
