package searchmetrics.crypto;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import searchmetrics.crypto.fetch.AverageFetcher;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * This is the main task of the application. {@see FetchAndStoreTask} will execute for the configurable
 * delay and fetch all ticket information from the crypto APIs then save an average to the {@see TickRepo}.
 */
@Slf4j
@Component
public class FetchAndStoreTask implements Runnable {

    @Autowired
    AverageFetcher fetcher;

    @Autowired
    private TickRepo tickRepo;

    @Value("${searchmetrics.crypto}")
    Currency.Crypto crypto;

    @Value("${searchmetrics.fiats}")
    Currency.Fiat[] fiats;

    // TODO find a way to remove the 30 second waiting period for Elasticsearch to start up.
    @Override
    @Scheduled(initialDelay = 30000, fixedDelayString = "${searchmetrics.intervalMillis}")
    public void run() {
        log.info("Running scheduled task to fetch and then store the crypto configured markets.");

        Map<Currency.Fiat, Double> priceMap = this.fetcher.fetch(crypto, fiats).getPriceMap();

        Tick tick = Tick.builder()
                .id(String.valueOf(UUID.randomUUID())) // init
                .date(new Date()) // init
                .crypto(crypto)
                .priceMap(priceMap)
                .build();

        this.tickRepo.save(tick);
    }
}
