package searchmetrics.crypto.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import searchmetrics.crypto.Currency;
import searchmetrics.crypto.Tick;
import searchmetrics.crypto.TickBucketResponse;
import searchmetrics.crypto.TickService;

import java.util.List;

@RestController
@RequestMapping("tick")
public class CryptoRestController {
    @Autowired
    private TickService tickService;

    /**
     * Returns the last tick values for the provided {@see Currency.Fiat}
     *
     * @param crypto the cyrpto currency to convert
     * @param fiats  the fiats to convert to
     * @return
     */
    @GetMapping("last")
    public Tick getLast(@RequestParam(defaultValue = "BTC") Currency.Crypto crypto,
                        @RequestParam(defaultValue = "USD") Currency.Fiat[] fiats) {
        return this.tickService.getLast(crypto, fiats);
    }

    /**
     * @param crypto the cyrpto currency to convert
     * @param fiat   the fiats to convert to
     * @return
     */
    @GetMapping("between")
    public List<Tick> getChartAll(@RequestParam(defaultValue = "BTC") Currency.Crypto crypto,
                                  @RequestParam(defaultValue = "USD") Currency.Fiat fiat,
                                  @RequestParam(required = false) Long startDateMillis,
                                  @RequestParam(required = false) Long endDateMillis) {

        return this.tickService.findTicks(crypto, fiat, startDateMillis, endDateMillis);
    }

    /**
     * @param crypto   the cyrpto currency to convert
     * @param fiat     the fiats to convert to
     * @param interval the interval to bucket and average over the
     * @return
     */
    @GetMapping("bucketsByInterval")
    public TickBucketResponse getChart(@RequestParam(defaultValue = "BTC") Currency.Crypto crypto,
                                       @RequestParam(defaultValue = "USD") Currency.Fiat fiat,
                                       @RequestParam(required = false) Long startDateMillis,
                                       @RequestParam(required = false) Long endDateMillis,
                                       @RequestParam(defaultValue = "1h") String interval) {

        // TODO make overloading methods to default instead of the controller

        return this.tickService.findTickBuckets(crypto, fiat, startDateMillis, endDateMillis, interval);
    }
}
