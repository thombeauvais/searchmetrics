package searchmetrics.crypto;

import lombok.extern.slf4j.Slf4j;

import java.util.function.Predicate;

/**
 * This class is simply here to make is a bit more readable for MVP.
 * <p>
 * Either these enums will need to be updated with each additional currency or the implementation will have to be
 * switch to use a basic {@code String} which will require better error checking.
 */
public interface Currency {

    @Slf4j
    enum Fiat implements Currency {
        AUD,
        BRL,
        CAD,
        CHF,
        CLP,
        CNY,
        DKK,
        EUR,
        GBP,
        HKD,
        INR,
        ISK,
        JPY,
        KRW,
        NZD,
        PLN,
        RUB,
        SEK,
        SGD,
        THB,
        TWD,
        USD;

        public final static Predicate<String> isValid = (name) -> {
            try {
                Currency.Fiat.valueOf(name);

                return true;
            } catch (IllegalArgumentException ex) {
                log.warn("Currency.Fiat needs to be added {}", name);

                return false;
            }
        };
    }

    @Slf4j
    enum Crypto implements Currency {
        BTC,
        ETH;
    }

}

