package searchmetrics;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.springframework.util.StringUtils.isEmpty;

@Slf4j
@ComponentScan
@SpringBootConfiguration
@EnableAutoConfiguration
@EnableScheduling
public class SpringConfiguration {
    public static void main(String[] args) {
        org.springframework.boot.SpringApplication.run(SpringConfiguration.class, args);
    }

    @Bean
    @SneakyThrows
    Client client() {
        Settings settings = Settings.builder()
                .put("cluster.name", "searchmetrics")
                .build();

        String elasticHost = isEmpty(System.getenv("ELASTIC_HOST")) ? "localhost" : System.getenv("ELASTIC_HOST");

        TransportClient client = new PreBuiltTransportClient(settings);
        client.addTransportAddress(new TransportAddress(InetAddress.getByName(elasticHost), 9300));

        return client;
    }

    @Bean
    RestTemplate restTemplate() {
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();

        //Add the Jackson Message converter
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();

        // Note: here we are making this converter to process any kind of response,
        // not only application/*json, which is the default behaviour
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
        messageConverters.add(converter);

        return new RestTemplateBuilder()
                .messageConverters(messageConverters)
                .build();
    }
}
